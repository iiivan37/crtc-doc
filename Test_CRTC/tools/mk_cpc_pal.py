import sys
import json

def read_config(config_file):
    with open(config_file) as json_file:
        data = json.load(json_file)
        return data["values"]

def make_palette(data):
    return "\n".join([f"\tdb Color.fm_{fm_color}" for fm_color in data])

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("input and output required")
        sys.exit(1)

    in_file = sys.argv[1]
    ou_file = sys.argv[2]

    OUT = make_palette(read_config(in_file))
    with open(ou_file, 'w') as file:
        file.write(OUT)