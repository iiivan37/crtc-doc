import sys
import csv


def read_map_csv(file):
    def normalize(clm):
        value = int(clm)
        return value if value >= 0 else value + 256

    with open(file) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        res = []
        for row in csv_reader:
          res.append("db " + ",".join([str(normalize(column)) for column in row]))
        
        return "\n".join(res)

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("input and output required")
        sys.exit(1)

    in_file = sys.argv[1]
    ou_file = sys.argv[2]

   
    OUT = read_map_csv(in_file)
    with open(ou_file, 'w') as file:
        file.write(OUT)

        