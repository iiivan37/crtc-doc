#!/bin/bash

rasm ./src/test_reg3_scanlines_shifting.asm
rasm ./src/test_reg3_HCsync.asm
rasm ./src/test_reg1_reg3_HSync.asm
rasm ./src/test_reg1_reg3_HSync2.asm
rasm ./src/test_reg1_overflow_border.asm
rasm ./src/test_reg3_multimode.asm
rasm ./src/test_reg3_VSync_size.asm
rasm ./src/test_reg1_changing.asm

mv ./*.dsk ./dist

cp ./dist/*.dsk /media/claire/CPC 2> /dev/null

 