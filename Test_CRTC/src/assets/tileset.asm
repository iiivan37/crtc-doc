; Data created with Img2CPC - (c) Retroworks - 2007-2015
; Tile _tile_00 - 4x8 pixels, 2x8 bytes.
_tile_00:
DEFB #c0, #c0
DEFB #40, #40
DEFB #00, #00
DEFB #00, #80
DEFB #80, #aa
DEFB #aa, #00
DEFB #00, #ff
DEFB #55, #80

; Tile _tile_01 - 4x8 pixels, 2x8 bytes.
_tile_01:
DEFB #c0, #c0
DEFB #00, #40
DEFB #00, #00
DEFB #40, #00
DEFB #55, #40
DEFB #00, #55
DEFB #ff, #00
DEFB #40, #aa

; Tile _tile_02 - 4x8 pixels, 2x8 bytes.
_tile_02:
DEFB #80, #aa
DEFB #aa, #00
DEFB #00, #55
DEFB #ff, #80
DEFB #c0, #c0
DEFB #40, #40
DEFB #00, #00
DEFB #00, #80

; Tile _tile_03 - 4x8 pixels, 2x8 bytes.
_tile_03:
DEFB #55, #40
DEFB #00, #55
DEFB #aa, #00
DEFB #40, #ff
DEFB #c0, #c0
DEFB #00, #40
DEFB #00, #00
DEFB #40, #00

; Tile _tile_04 - 4x8 pixels, 2x8 bytes.
_tile_04:
DEFB #0c, #0c
DEFB #04, #04
DEFB #00, #00
DEFB #00, #08
DEFB #08, #55
DEFB #00, #00
DEFB #00, #55
DEFB #00, #08

; Tile _tile_05 - 4x8 pixels, 2x8 bytes.
_tile_05:
DEFB #0c, #0c
DEFB #00, #04
DEFB #00, #00
DEFB #04, #00
DEFB #aa, #04
DEFB #00, #aa
DEFB #aa, #00
DEFB #04, #aa

; Tile _tile_06 - 4x8 pixels, 2x8 bytes.
_tile_06:
DEFB #08, #00
DEFB #00, #00
DEFB #00, #00
DEFB #00, #08
DEFB #0c, #0c
DEFB #04, #04
DEFB #00, #00
DEFB #00, #08

; Tile _tile_07 - 4x8 pixels, 2x8 bytes.
_tile_07:
DEFB #aa, #04
DEFB #00, #aa
DEFB #aa, #00
DEFB #04, #aa
DEFB #0c, #0c
DEFB #00, #04
DEFB #00, #00
DEFB #04, #00

; Tile _tile_08 - 4x8 pixels, 2x8 bytes.
_tile_08:
DEFB #cc, #cc
DEFB #44, #44
DEFB #00, #00
DEFB #00, #88
DEFB #88, #aa
DEFB #00, #00
DEFB #00, #ff
DEFB #55, #88

; Tile _tile_09 - 4x8 pixels, 2x8 bytes.
_tile_09:
DEFB #cc, #cc
DEFB #00, #44
DEFB #00, #00
DEFB #44, #00
DEFB #55, #44
DEFB #00, #ff
DEFB #ff, #00
DEFB #44, #aa

; Tile _tile_10 - 4x8 pixels, 2x8 bytes.
_tile_10:
DEFB #88, #55
DEFB #ff, #00
DEFB #00, #ff
DEFB #ff, #88
DEFB #cc, #cc
DEFB #44, #44
DEFB #00, #00
DEFB #00, #88

; Tile _tile_11 - 4x8 pixels, 2x8 bytes.
_tile_11:
DEFB #aa, #44
DEFB #00, #00
DEFB #ff, #00
DEFB #44, #ff
DEFB #cc, #cc
DEFB #00, #44
DEFB #00, #00
DEFB #44, #00

; Tile _tile_12 - 4x8 pixels, 2x8 bytes.
_tile_12:
DEFB #30, #30
DEFB #30, #10
DEFB #00, #00
DEFB #00, #20
DEFB #20, #00
DEFB #55, #00
DEFB #00, #ff
DEFB #ff, #20

; Tile _tile_13 - 4x8 pixels, 2x8 bytes.
_tile_13:
DEFB #30, #30
DEFB #30, #30
DEFB #00, #10
DEFB #10, #00
DEFB #ff, #10
DEFB #10, #aa
DEFB #ff, #10
DEFB #10, #aa

; Tile _tile_14 - 4x8 pixels, 2x8 bytes.
_tile_14:
DEFB #20, #55
DEFB #00, #20
DEFB #20, #ff
DEFB #ff, #20
DEFB #30, #30
DEFB #30, #30
DEFB #20, #00
DEFB #00, #20

; Tile _tile_15 - 4x8 pixels, 2x8 bytes.
_tile_15:
DEFB #aa, #10
DEFB #10, #ff
DEFB #aa, #10
DEFB #10, #ff
DEFB #30, #30
DEFB #30, #30
DEFB #00, #10
DEFB #10, #00

