
include "macros.asm"

    print '########################################################################'

;################################################################################################
    header 'Code',#500,#3FF0
;################################################################################################

start:

    ld a,2:call #bc0e ; Mode 2

    ld a,'M':call #bb5a
    ld a,'o':call #bb5a
    ld a,'d':call #bb5a
    ld a,'e':call #bb5a
    ld a,' ':call #bb5a
    ld a,'2':call #bb5a

    ld a,#40:call #bc08
    ld a,1:call #bc0e ; Mode 1
    ld a,#c0:call #bc08

    ld a,15:call #bb6f ; locate n,12

    ld a,'M':call #bb5a
    ld a,'o':call #bb5a
    ld a,'d':call #bb5a
    ld a,'e':call #bb5a
    ld a,' ':call #bb5a
    ld a,'1':call #bb5a

    ld a,#40:call #bc08
    ld a,0:call #bc0e ; Mode 0
    ld a,#c0:call #bc08

    ld a,15:call #bb6f ; locate n,15

    ld a,'M':call #bb5a
    ld a,'o':call #bb5a
    ld a,'d':call #bb5a
    ld a,'e':call #bb5a
    ld a,' ':call #bb5a
    ld a,'0':call #bb5a

    INIT_TEST

    jr $


;#########################################################

    outcrtc 1,31
    outcrtc 6,40

    outcrtc 7,20
    outcrtc 2,16
    outcrtc 0,31
    outcrtc 3,7

loop:
    jr loop

;################################################################################################
    footer 'Code',#500,#3FF0
;################################################################################################
end:
    total_length equ end-start
    print '                Total length : ', {hex4} total_length
;===============================================
SAVE 'test.bin',start,end-start,DSK,'test_reg3_multimode.dsk'
