
include "macros.asm"

    print '########################################################################'

;################################################################################################
    header 'Code',#500,#3FF0
;################################################################################################

start:

    INIT_TEST

    DRAW_TEST_PATTERN_FOR_CHARS_COUNT 1,31

    ;Doubles the height of the line of sprites

    memcpy #F800,#F000+62,62
    memcpy #F000,#E000+62,62
    memcpy #E800,#D000+62,62
    memcpy #E000,#C000+62,62
    memcpy #D800,#F000,62
    memcpy #D000,#E000,62
    memcpy #C800,#D000,62

    memcpy #C000,#C000+124,#7FF-124
    memcpy #D000,#D000+124,#7FF-124
    memcpy #E000,#E000+124,#7FF-124
    memcpy #F000,#F000+124,#7FF-124

    memcpy #C000,#C800,#7FF
    memcpy #D000,#D800,#7FF
    memcpy #E000,#E800,#7FF
    memcpy #F000,#F800,#7FF

;#########################################################

    ei

    ld b,#f5
vbl:
    halt
    in a,(c)
    rra
    jr nc,vbl
    di

    outcrtc 1,31
    outcrtc 6,40

    outcrtc 7,20
    outcrtc 2,16
    outcrtc 0,31

loop:
    outcrtc 3,7
    delay 32-14
    outcrtc 3,2
    delay 32-14-3
    jr loop

;################################################################################################
    footer 'Code',#500,#3FF0
;################################################################################################
end:
    total_length equ end-start
    print '                Total length : ', {hex4} total_length
;===============================================
SAVE 'test.bin',start,end-start,DSK,'test_reg1_reg3_HSync2.dsk'
