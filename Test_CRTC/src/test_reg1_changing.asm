
include "macros.asm"

    print '########################################################################'

;################################################################################################
    header 'Code',#500,#3FF0
;################################################################################################

start:
    INIT_TEST

    outcrtc 3,6

    ld hl,#c000        : ld a,01 : call printnum
    ld hl,#c000+80*01  : ld a,02 : call printnum
    ld hl,#c000+80*02  : ld a,03 : call printnum
    ld hl,#c000+80*03  : ld a,04 : call printnum
    ld hl,#c000+80*04  : ld a,05 : call printnum
    ld hl,#c000+80*05  : ld a,06 : call printnum
    ld hl,#c000+80*06  : ld a,07 : call printnum
    ld hl,#c000+80*07  : ld a,08 : call printnum
    ld hl,#c000+80*08  : ld a,09 : call printnum
    ld hl,#c000+80*09  : ld a,10 : call printnum
    ld hl,#c000+80*10  : ld a,11 : call printnum
    ld hl,#c000+80*11  : ld a,12 : call printnum

;#########################################################
loop:
    ei
    setborder 18

    ld b,#f5
wait_vsync:
    halt
    in a,(c)                    ; 4
    rra                         ; 1
    jr nc,wait_vsync            ; 3 if yes, 2 otherwise

    halt : setborder 5
    halt : setborder 12

    delay 32

    outcrtc 1,0 : delay 64-14
    outcrtc 1,40 : delay 64-14
    outcrtc 1,0 : delay 64-14
    outcrtc 1,40 : delay 64-14
    outcrtc 1,0 : delay 64-14
    outcrtc 1,40 : delay 64-14

    delay 64*8

    outcrtc 1,0 : delay 64*8-14
    outcrtc 1,40

    jp loop

printnum:
    printhex
    ret

;################################################################################################
    footer 'Code',#500,#3FF0
;################################################################################################
end:
    total_length equ end-start
    print '                Total length : ', {hex4} total_length
;===============================================
SAVE 'test.bin',start,end-start,DSK,'test_reg1_changing.dsk'
