
include 'macros.asm'

    print '########################################################################'

;################################################################################################
    header 'Code',#500,#3FF0
;################################################################################################
start:

    INIT_TEST
    DRAW_TEST_PATTERN_FOR_SCANLINE_COUNT 40,16

    outcrtc 6,39
    outcrtc 1,20
    outcrtc 2,36

;#########################################################
Frame:
    ei
    ld b,#f5
.loop:
    halt
    in a,(c)
    rra
    jr nc,.loop

    outcrtc 3,6

    ld bc,#bd04
    halt ; 1
    out (c),c : setink 0,12 : setink 0,20

    di
    delay 16*64-14*3

    outcrtc 3,6 : setink 0,12 : setink 0,20 : delay 16*64-14*3
    outcrtc 3,4 : setink 0,12 : setink 0,20 : delay 16*64-14*3
    outcrtc 3,5 : setink 0,12 : setink 0,20 : delay 16*64-14*3
    outcrtc 3,4 : setink 0,12 : setink 0,20 : delay 16*64-14*3
    outcrtc 3,5 : setink 0,12 : setink 0,20 : delay 16*64-14*3
    outcrtc 3,6 : setink 0,12 : setink 0,20 : delay 16*64-14*3
    outcrtc 3,5 : setink 0,12 : setink 0,20 : delay 16*64-14*3
    outcrtc 3,6 : setink 0,12 : setink 0,20 : delay 16*64-14*3

    outcrtc 3,4 : setink 0,12 : setink 0,20 : delay 64+64-14*3
    outcrtc 3,5 : setink 1,12 : setink 1,11 : delay 14*64-14*3

    outcrtc 3,4 : setink 0,12 : setink 0,20 : delay 16*64-14*3

    outcrtc 3,5 : setink 0,12 : setink 0,20 : delay 64-14*3
    outcrtc 3,4 : delay 64-14
    outcrtc 3,5 : delay 64-14
    outcrtc 3,4 : delay 64-14
    outcrtc 3,5 : delay 64-14
    outcrtc 3,4 : delay 64-14
    outcrtc 3,5 : delay 64-14
    outcrtc 3,4 : delay 64-14
    outcrtc 3,5 : delay 64-14
    outcrtc 3,4 : delay 64-14
    outcrtc 3,5 : delay 64-14
    outcrtc 3,4 : delay 64-14
    outcrtc 3,5 : delay 64-14
    outcrtc 3,4 : delay 64-14
    outcrtc 3,5 : setink 0,12 : setink 0,20 : delay 16*64-14*3

    outcrtc 3,6 : setink 0,12 : setink 0,20 : delay 64-14*3
    outcrtc 3,5 : delay 64-14
    outcrtc 3,6 : delay 64-14
    outcrtc 3,5 : delay 64-14
    outcrtc 3,6 : delay 64-14
    outcrtc 3,5 : delay 64-14
    outcrtc 3,6 : delay 64-14
    outcrtc 3,5 : delay 64-14
    outcrtc 3,6 : delay 64-14
    outcrtc 3,5 : delay 64-14
    outcrtc 3,6 : delay 64-14
    outcrtc 3,5 : delay 64-14
    outcrtc 3,6 : delay 64-14
    outcrtc 3,5 : delay 64-14
    outcrtc 3,6 : setink 0,12 : setink 0,20

    jp Frame
;################################################################################################
    footer 'Code',#500,#3FF0
;################################################################################################
end:
    total_length equ end-start
    print '                Total length : ', {hex4} total_length
;===============================================
SAVE 'test.bin',start,end-start,DSK,'test_reg3_scanlines_shifting.dsk'
