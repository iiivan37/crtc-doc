print '           .ymNy`o+                      .:`ooo.'
print '         .NMMM*  :Md.                   yM` `NMMdo'
print '         MMMMd   `MMh                  hMm   :@MMMh'
print '        :MMMMo   .MMM:                sMMd    mMMMM`'
print '         *NMMs   yMMMy               .MMMN    @MMMo'
print '          `+hd. oMNdy`               -dMMMo. :MMh:'
print '`-              ``                    `*MM* .**`'
print ' os`                                                   .+`'
print '  .+y-                                                `oh'
print '   `yy+\.                                          :y+``'
print '     `-oMd++-                                    :+yN-:'
print '       `:./N+Ndhs\\:-....-/ohhdyo/-.     .-:+ohNs/+.s`'
print '         `+\hy/Ny:Mh+MN:MmoMMomMM/Nd+MM+Mm:Md.o/.+-`'
print '            `+o.mhomoMy/:+`yo`-s/.+.ohNyy-m::Ny/`'
print '                `-=o:.hhhh+N//+y+hN/Nh:hy-+-`'
print ' '
print ' 2020, CheshireCat, going back to her roots !'
print ' '
;=============================================================
; Thanks to Roudoudou for RASM ;-)
; Thanks to GurneyH for a late night chat which gave birth to some ideas !
; Thanks to Grim for pointing a lot of discrepancies in the document and a lot of new paths to explore
;=============================================================

;=========================
; Sends a value to a register of the CRTC
;-------------------------
; Duration : 14 NOPs
;=========================
macro OUTCRTC register,value
	ld bc,#bc00 + {register}
	out (c),c
	ld bc,#bd00 + {value}
	out (c),c
mend
;=========================
; Defines the color of the border
; The ASM equivalent of the "BORDER" Basic command
;-------------------------
; Duration : 14 NOPs
;=========================
macro SETBORDER value
    ld bc, #7f10
    out (c),c
    ld bc, #7f40 + {value}
    out (c),c
mend
;=========================
; Defines a color to an ink
; The ASM equivalent of the "INK" Basic command
;-------------------------
; Duration : 14 NOPs
;=========================
macro SETINK number,value
    ld bc, #7f00 + {number}
    out (c),c
    ld bc, #7f40 + {value}
    out (c),c
mend
;=========================
; Waits for a given number of NOPs
; /!\ WARNING /!\ Register B is used
;=========================
macro DELAY number	
	assert {number}>0, 'Number of NOPs to wait is negative'

	nop_count={number}

;	print 'Delaying for', {int} nop_count, 'NOPs.'

	if (nop_count<6)
		remaining=nop_count
		djnz_delay=0
	else
		nop_count=nop_count-2 ; 2 nops are used for the LD B,xx

		djnz_delay=floor(nop_count/4)
		if (((djnz_delay<<2)-nop_count)==3)
			djnz_delay=djnz_delay+1
		endif
	
		remaining=nop_count-(((djnz_delay-1)<<2)+3)
	endif

	if (djnz_delay>0)
		ld b,djnz_delay  ; 2 nops
		djnz $			 ; 4 nops if B>0, 3 nops if B=0
	endif

	if (remaining<>0)
		defs remaining,0
	endif

;	print 'macro DELAY nop_count', {int} nop_count
;	print 'macro DELAY djnz_delay', {int} djnz_delay
;	print 'macro DELAY remaining', {int} remaining
mend
;=========================
macro MEMCPY source,dest,lng
	if {source} > {dest}
		ld hl,{source}+{lng}-1
		ld de,{dest}+{lng}-1
		ld bc,{lng}
		lddr
	else
		ld hl,{source}
		ld de,{dest}
		ld bc,{lng}
		ldir
	endif
mend
;=========================
; Initialization - Defines SP, sets the inks, deactivate the interruptions
;=========================
macro INIT_TEST
    di
    ld sp,#3fff

    ld bc,#7f00+%10001101   ; mode 1
    out (c),c

    setborder 12    ; Brigth Red

    setink 0,4      ; Blue
    setink 1,11     ; Bright White
    setink 2,10     ; Bright Yellow
    setink 3,18     ; Bright Green

    ld hl,#c9fb
    ld (#38),hl
    ei
mend
;=========================
; Draws a test pattern on the screen
; uses the screen in &C000
; parms : Height (expressed in chars), Width (expressed in chars)
;=========================
macro DRAW_TEST_PATTERN_FOR_SCANLINE_COUNT height,width
	screen_height={height}
	screen_width={width}

    ld a,240    ; Color #1
    ld hl,#c000
    ld (hl),a

    ld de,#c001
    ld bc,#7ff
    ldir
    
    ld a,15     ; Color #2
    ld hl,#d000
    ld de,screen_width*2

    ld b,screen_height
.loopline2
    ld (hl),a
    add hl,de
    djnz .loopline2

    ld a,255     ; Color #3
    ld hl,#e000
    ld b,screen_height
.loopline3
    ld (hl),a
    add hl,de
    djnz .loopline3

    ld a,15     ; Color #2
    ld hl,#f000
    ld b,screen_height
.loopline4
    ld (hl),a
    add hl,de
    djnz .loopline4

;-------------

    ld a,128
    ld hl,#c800+2
    ld b,screen_height
.loopline5
    ld (hl),a
    add hl,de
    djnz .loopline5

    ld hl,#d000+2
    ld b,screen_height
.loopline6
    ld (hl),a
    add hl,de
    djnz .loopline6

    ld hl,#d800+2
    ld b,screen_height
.loopline7
    ld (hl),a
    add hl,de
    djnz .loopline7

    ld hl,#e000+2
    ld b,screen_height
.loopline8
    ld (hl),a
    add hl,de
    djnz .loopline8

    ld hl,#e800+2
    ld b,screen_height
.loopline9
    ld (hl),a
    add hl,de
    djnz .loopline9

    ld hl,#f000+2
    ld b,screen_height
.looplineA
    ld (hl),a
    add hl,de
    djnz .looplineA

    ld hl,#f800+2
    ld b,screen_height
.looplineB
    ld (hl),a
    add hl,de
    djnz .looplineB

    ld a,255
    ld (#c008),a
mend

;=========================
; Draws a test pattern on the screen
; uses the screen in &C000
; parms : Height (expressed in chars), Width (expressed in chars)
;=========================
macro DRAW_TEST_PATTERN_FOR_CHARS_COUNT height,width
	screen_height={height}
	screen_width={width}

	ld hl,#c000

.newline:
	ld a,0
.loopline:
	push af
	printhex
	pop af
	inc a
	cp screen_width
	jp nz,.loopline

.Linecount:
	ld a,screen_height
	dec a
	ld (.Linecount+1),a
	cp 0
	jp nz,.newline
mend
;=========================
; Prints a hex number between 0 and #ff, in mode 1, one byte in width, 6 lines in height
; hl = Address
; a = value
;=========================
macro printhex

	ld d,a
	and #0f
	ld (.lownibble+1),a
	ld a,#f0
	and d
	rrca
	rrca
	rrca
	rrca
	ld (.highnibble+1),a
	
	ld a,%00001111 ; 4 pixels color #2
	ld (hl),a
	ld a,8
	add a,h
	ld h,a

.highnibble:
	ld a,0
	call .printnumber
	
	ld a,8
	add a,h
	ld h,a
	xor a
	ld (hl),a

	ld de,-7*#800+1
	add hl,de

	xor a
	ld (hl),a
	ld a,8
	add a,h
	ld h,a

.lownibble:
	ld a,0
	call .printnumber

	ld a,8
	add a,h
	ld h,a
	ld a,#ff ; 4 pixels color #3
	ld (hl),a

	ld de,-7*#800+1
	add hl,de

	jp .Theend

.printnumber:
	ld c,a
	add a,c
	add a,c
	add a,c
	add a,c
	add a,c

	ld bc,.spritestab
	add a,c
	ld c,a
	ld a,0
	adc a,b
	ld b,a

	ld d,8

	ld a,(bc)
	ld (hl),a

	repeat 5
		ld a,h
		add a,d
		ld h,a
		inc bc
		ld a,(bc)
		ld (hl),a
	rend
	ret

.spritestab:
	db %01000000
	db %10100000
	db %10100000
	db %10100000
	db %10100000
	db %01000000

	db %01000000
	db %11000000
	db %01000000
	db %01000000
	db %01000000
	db %11100000

	db %01000000
	db %10100000
	db %00100000
	db %01000000
	db %10000000
	db %11100000

	db %11000000
	db %00100000
	db %11000000
	db %00100000
	db %00100000
	db %11000000

	db %10000000
	db %10000000
	db %10100000
	db %11100000
	db %00100000
	db %00100000

	db %11100000
	db %10000000
	db %11000000
	db %00100000
	db %00100000
	db %11000000

	db %01100000
	db %10000000
	db %11000000
	db %10100000
	db %10100000
	db %01000000

	db %11100000
	db %00100000
	db %00100000
	db %01000000
	db %01000000
	db %01000000

	db %01000000
	db %10100000
	db %01000000
	db %10100000
	db %10100000
	db %01000000

	db %01000000
	db %10100000
	db %01100000
	db %00100000
	db %10100000
	db %01000000

	db %01000000
	db %10100000
	db %11100000
	db %10100000
	db %10100000
	db %10100000

	db %11000000
	db %10100000
	db %11000000
	db %10100000
	db %10100000
	db %11000000

	db %01000000
	db %10100000
	db %10000000
	db %10000000
	db %10100000
	db %01000000

	db %11000000
	db %10100000
	db %10100000
	db %10100000
	db %10100000
	db %11000000

	db %11100000
	db %10000000
	db %11100000
	db %10000000
	db %10000000
	db %11100000

	db %11100000
	db %10000000
	db %11100000
	db %10000000
	db %10000000
	db %10000000

.Theend:
mend

;=========================
; Header template
;=========================
macro header window_name,start_address,end_address
;print '########################################################################'
	org {start_address}
	print '       Start of the',{window_name},':',{hex}{start_address},'->',{hex}{end_address}
	print '########################################################################'
	print ' '
mend
;=========================
; Footer template
;=========================
macro footer window_name,start_address,end_address
	print ' '
	print 'End of the',{window_name},':',{hex}$
	overflow = $-{end_address}
	assert ($<{end_address}), {window_name},'OVERFLOWED of',{hex}overflow,'bytes'
	if ($>{end_address})
		print {window_name},'OVERFLOWED of',{hex}overflow,'bytes'
		assert $<{end_address}, 'OVERFLOW'
	endif
	
	bytes_free = {end_address}-$
	print 'Free space at the end of the window :',{hex}bytes_free,'(',{int}bytes_free,')'
	print '########################################################################'
mend



