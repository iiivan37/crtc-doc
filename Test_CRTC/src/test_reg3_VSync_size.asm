
include "macros.asm"

    print '########################################################################'

;################################################################################################
    header 'Code',#500,#3FF0
;################################################################################################

start:
    INIT_TEST

;#########################################################

vsync_test_loop:
    ld a,15
    inc a
    and #0f
    cp 5
    jr nc,next
    cp 0
    jr z,next
    ld a,5
next:
    ld (vsync_test_loop+1),a

    rlca
    rlca
    rlca
    rlca

    ld (nlines_mul_16+1),a

    add a,6

    ld bc,#bc03
    out (c),c
    inc b
    out (c),a
 
    ld de,0
    ld b,#f5
wait_vsync:
    halt
    in a,(c)                    ; 4
    rra                         ; 1
    jr nc,wait_vsync            ; 3 if yes, 2 otherwise
measure_vsync_length:
    inc de                      ; 2
    in a,(c)                    ; 4
    rra                         ; 1
    jr c, measure_vsync_length  ; 3 if yes, 2 otherwise

    ld hl,64*2
    add hl,de
    add hl,de
    add hl,de
    add hl,de
    add hl,de

    add hl,de
    add hl,de
    add hl,de
    add hl,de
    add hl,de

    push hl
    push hl
    ld hl,#c000
    ld d,0

nlines_mul_16:
    ld l,0
    ld e,l ; e=l=nb lines*16
    
    rl e
    rl d  ; *32
    rl e
    rl d  ; *64

    add hl,de
    
    pop bc
    push bc
    ld a,b
    call printnum
    pop bc
    ld a,c
    call printnum

    inc hl
    inc hl
    
    pop bc
    sla c
    rl b
    sla c
    rl b

    ld a,b
    
    call printnum

jp vsync_test_loop

printnum:
    printhex
    ret

;################################################################################################
    footer 'Code',#500,#3FF0
;################################################################################################
end:
    total_length equ end-start
    print '                Total length : ', {hex4} total_length
;===============================================
SAVE 'test.bin',start,end-start,DSK,'test_reg3_VSync_size.dsk'
