
include "macros.asm"

    print '########################################################################'

;################################################################################################
    header 'Code',#500,#3FF0
;################################################################################################

start:

    INIT_TEST

    DRAW_TEST_PATTERN_FOR_CHARS_COUNT 1,32

    memcpy #F800,#F000+64,64
    memcpy #F000,#E000+64,64
    memcpy #E800,#D000+64,64
    memcpy #E000,#C000+64,64
    memcpy #D800,#F000,64
    memcpy #D000,#E000,64
    memcpy #C800,#D000,64

    memcpy #C000,#C000+128,#7FF-128
    memcpy #D000,#D000+128,#7FF-128
    memcpy #E000,#E000+128,#7FF-128
    memcpy #F000,#F000+128,#7FF-128

    memcpy #C000,#C800,#7FF
    memcpy #D000,#D800,#7FF
    memcpy #E000,#E800,#7FF
    memcpy #F000,#F800,#7FF

;#########################################################

    outcrtc 1,32

    outcrtc 6,40

    outcrtc 7,20
    outcrtc 2,24
    outcrtc 0,31
    outcrtc 3,7

loop:
    jr loop

;################################################################################################
    footer 'Code',#500,#3FF0
;################################################################################################
end:
    total_length equ end-start
    print '                Total length : ', {hex4} total_length
;===============================================
SAVE 'test.bin',start,end-start,DSK,'test_reg1_overflow_border.dsk'
